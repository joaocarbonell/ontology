/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsws.automotiveontology.beans;

import com.dsws.automotiveontology.ontologias.OntoClass;
import com.dsws.automotiveontology.ontologias.OntoIndividual;
import java.io.Serializable;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;
 
@ManagedBean(name = "BeanIndividuos")
@SessionScoped
public class BeanIndividuos implements Serializable{
     
    private OntoIndividual ontoIndividualSistemaDeIluminacaoInterna;
    private OntoIndividual ontoIndividualSistemaDeIluminacaoExterna;
    private OntoIndividual ontoIndividualSistemaDeClimatizacao;
    private OntoIndividual ontoIndividualSistemaDeEstacionamento;
    private ArrayList<OntoIndividual> ontoIndividuos;
  
    public BeanIndividuos(){
        ontoIndividuos = new ArrayList<OntoIndividual>();
        ontoIndividualSistemaDeIluminacaoInterna = new OntoIndividual("http://www.unipampa.edu.br/ontology/OntologiaVeiculo.owl#SistemaIlumInterna");
        ontoIndividualSistemaDeIluminacaoExterna = new OntoIndividual("http://www.unipampa.edu.br/ontology/OntologiaVeiculo.owl#SistemaIluminaçãoExterna");
        ontoIndividualSistemaDeClimatizacao = new OntoIndividual("http://www.unipampa.edu.br/ontology/OntologiaVeiculo.owl#Sistema_de_Climatização");
        ontoIndividualSistemaDeEstacionamento = new OntoIndividual("http://www.unipampa.edu.br/ontology/OntologiaVeiculo.owl#Sistema_de_Estacionamento");
        ontoIndividuos.add(ontoIndividualSistemaDeIluminacaoInterna);   
        ontoIndividuos.add(ontoIndividualSistemaDeIluminacaoExterna);   
        ontoIndividuos.add(ontoIndividualSistemaDeClimatizacao);   
        ontoIndividuos.add(ontoIndividualSistemaDeEstacionamento);   
        }
                
    public void onTabChange(TabChangeEvent event) {
        FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
         
    public void onTabClose(TabCloseEvent event) {
        FacesMessage msg = new FacesMessage("Tab Closed", "Closed tab: " + event.getTab().getTitle());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
 
    public ArrayList<OntoIndividual> getOntoIndividuos() {
        
          
          System.out.println(ontoIndividuos.get(2).getObjectProperty().get(0).getValues().get(0).getObjectProperty().get(2).getLabel());
          System.out.println(ontoIndividuos.get(2).getObjectProperty().get(0).getValues().get(0).getObjectProperty().get(2).getValues().get(0).getLabel());
        return ontoIndividuos;
    }

    public void setOntoIndividuos(ArrayList<OntoIndividual> ontoIndividuos) {
        this.ontoIndividuos = ontoIndividuos;
    }
    
}
